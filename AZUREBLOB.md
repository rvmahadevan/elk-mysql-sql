REF: https://docs.microsoft.com/en-us/sql/relational-databases/polybase/polybase-configure-azure-blob-storage?view=sql-server-2017

## POLYBASE CONFIGURATION

## CONFIGURE POLYBASE ENVIRONMENT

#### STEP 1: CHECK for existing configuration

    EXEC sp_configure; 

#### STEP 2: CHECK for HADOOP Connectivity, if value = 0

    EXEC sp_configure @configname='hadoop connectivity';


#### STEP 3: Configure external tables to reference data on Hortonworks 2.1, 2.2, and 2.3 on Linux, and Azure blob storage  
  
    sp_configure @configname = 'hadoop connectivity', @configvalue = 7;  
    GO  
      
    RECONFIGURE  
    GO
    
#### STEP 4: Install JAVA and add JAVA_HOME environment variable
    

## CONNECT TO SQL SERVER

#### STEP 1: Create a master key on the database. This is required to encrypt the credential secret.

    CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'any password key';  

#### STEP 2: Create a database scoped credential for Azure blob storage.

    -- IDENTITY: any string (this is not used for authentication to Azure storage).  
    -- SECRET: your Azure storage account key. 
 
    CREATE DATABASE SCOPED CREDENTIAL GeAzureBlobStorageCredential
    WITH IDENTITY = 'gepolybase', Secret = 'Azure secret key to access blob';
    
#### STEP 3: Create an external data source with CREATE EXTERNAL DATA SOURCE..

    
    -- LOCATION:  Azure account storage account name and blob container name.  
    -- CREDENTIAL: The database scoped credential created above.  
    CREATE EXTERNAL DATA SOURCE GeAzureStorage with (  
          TYPE = HADOOP,
          LOCATION ='wasbs://<container name>@getestdatawarehouse.blob.core.windows.net',  
          CREDENTIAL = GeAzureBlobStorageCredential  
    );  
    
    
    
#### STEP 4: Create an external file format with CREATE EXTERNAL FILE FORMAT.

    -- FORMAT TYPE: Type of format in Hadoop (DELIMITEDTEXT,  RCFILE, ORC, PARQUET).
      
    CREATE EXTERNAL FILE FORMAT TextFileFormat
    WITH
    ( FORMAT_TYPE = DELIMITEDTEXT
    ,	FORMAT_OPTIONS	( FIELD_TERMINATOR = '|'
    ,	STRING_DELIMITER = ''
    ,	DATE_FORMAT	= 'yyyy-MM-dd HH:mm:ss.fff'
    ,	USE_TYPE_DEFAULT = FALSE
    )
    );
    GO

#### STEP 5: Create an external table.                        
                    
    CREATE EXTERNAL TABLE asb.tblname(
      [SeatID] [varchar](50)  NULL,
      [UserID] [varchar](50)  NULL,
      [Duration] [varchar](50)  NULL,
      [EndDate] [varchar](50)  NULL,
      [StartDate] [varchar](50)  NULL,
      [Frequency] [varchar](50)  NULL,
      [FrequencyUnit] [varchar](50)  NULL,
      [Type] [varchar](50)  NULL,
      [CoachType] [varchar](50)  NULL,
      [TotalSession] [varchar](50)  NULL,
      [Check_Sum] [varchar](50)  NULL,
      [CDCType] [varchar](50)  NULL,
      [ModifiedDate][varchar](50)  NULL,
      [SeatLotID] [varchar](50)  NULL
    ) WITH (LOCATION='/<folder in container>/',
          DATA_SOURCE = GeAzureStorage,  
          FILE_FORMAT = TextFileFormat ,
        REJECT_TYPE = VALUE,
        REJECT_VALUE = 0
    );
