
USE master
GO
-- Create the new database if it does not exist already
IF NOT EXISTS (
    SELECT [name]
        FROM sys.databases
        WHERE [name] = N'TestPOC'
)
CREATE DATABASE TestPOC;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employee](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
	[modified_date] [datetime] NULL,
    [inserted_date] [datetime] NULL,
PRIMARY KEY CLUSTERED
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employeelog](
	[userid] [int] NOT NULL,
	[firstname] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[userstatus] [nvarchar](50) NULL,
	[modified_date] [datetime] NULL,
	[inserted_date] [datetime] NULL,
PRIMARY KEY CLUSTERED
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-- Create the table in the specified schema

USE TestPOC;
DECLARE @NoOfRecords INT
DECLARE @i INT

SET  @NoOfRecords = 10
SET @i = 1

WHILE (@i<@NoOfRecords)
BEGIN
     INSERT INTO  dbo.employee  (firstname
                            ,email
                            ,status
                            , modified_date)

      SELECT     'New - ' +CONVERT(VARCHAR(10),@i) +' - Today Name - ' + CONVERT(VARCHAR(10),@i)
                ,'New - ' +CONVERT(VARCHAR(10),@i) +' - Email Name - ' + CONVERT(VARCHAR(10),@i) + '@testge.com'
                ,'New - ' +CONVERT(VARCHAR(10),@i) +' - Status - ' + CONVERT(VARCHAR(10),@i)
                , GETDATE()

        SET @i = @i+1
END