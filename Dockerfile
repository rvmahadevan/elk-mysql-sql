FROM docker.elastic.co/logstash/logstash:7.4.2
MAINTAINER Elastic Cloud

RUN /usr/share/logstash/bin/logstash-plugin install logstash-input-jdbc
RUN /usr/share/logstash/bin/logstash-plugin install logstash-output-jdbc

ADD ./lib/jars/mssql-jdbc-7.4.1.jre11.jar /usr/share/logstash/logstash-core/lib/jars/mssql-jdbc-7.4.1.jre11.jar
ADD ./lib/jars/mysql-connector-java-5.1.48.jar /usr/share/logstash/logstash-core/lib/jars/mysql-connector-java-5.1.48.jar

