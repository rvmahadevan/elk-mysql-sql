# ELK Stack for Realtime Plugin

## 1. Overview

This repo help you to create a ELK platform which will help you to sync realtime data between MYSQL and Sql Server, using Elastic search as a staging environment.

## 2. Technology

1. ElasticSearch (7.4.2) - Staging Storage
2. Kibana (7.4.2)- Visualization
3. Logstash (7.4.2) 
4. SQL Server 2016

### 2.1.  Dependency

pip install logstash-output-jdbc
pip install logstash-input-jdbc

## 3. Settings
1. Make sure you have proper SQL Server jdbc jar file downloaded and available in jars folder.

2. Use the script '/test/createobjects.sql' to create a new database (TestPoc) in SQL server and source and target tables to evaluate the pipelines. The script also contains insert query to populate the table with data.

## 3. Starting Docker

> Run docker-compose build 

## 4. Validate ELK Stack

After docker started successfully, check for below url's:

http://localhost:9200/ (ElasticSearch)
http://localhost:5601/ (Kibana)

Go to Kibana portal and add new index from Index Management page.

Refer 'test\elastic_dsl' for sample kibana query language.












